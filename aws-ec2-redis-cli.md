# Setup redis-cli without the whole Redis Server on AWS EC2

This fast tutorial will teach you how to install `redis-cli`on AWS EC2 without having to install the whole Redis Server. Firstly, SSH into your EC2 instance and run the following command:

`$ sudo yum install gcc` 

This may return an "already installed" message, but that's OK. After that, just run:

`$ wget http://download.redis.io/redis-stable.tar.gz && tar xvzf redis-stable.tar.gz && cd redis-stable && make && sudo cp src/redis-cli /usr/local/bin/ && sudo chmod 755 /usr/local/bin/redis-cli`

In order to connect to your remote server, use something like:

`$ redis-cli -h 192.268.34.32`

References:
  - http://docs.aws.amazon.com/AmazonElastiCache/latest/UserGuide/GettingStarted.ConnectToCacheNode.Redis.html
  - https://www.devops.zone/ha-performance-caching/installing-redis-cli-without-installing-server/